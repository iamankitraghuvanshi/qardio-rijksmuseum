#Author - Ankit Raghuvanshi
# Qardio Rijksmuseum - Code Challenge

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Common Links
#### 1. `http://localhost:4200`
#### 2. `http://localhost:4200/collections`
#### 3. `http://localhost:4200/events`
#### 4. `http://localhost:4200/chart`

## Automated Documentation 100%

Run `npm run compodoc` to generate automated documentation of the project (`/qardio-rijksmuseum/documentation/index.html`) or goto project folder `qardio-rijksmuseum > documentation > index.html`

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests

## Highlights of the project

### 1. Musesum Overview Component
- to display an overivew of the paintings.
### 2. Musesum Events Component
- to display daily events agenda (with date navigation)
### 3. Musesum Home Component
- to display homepage for navigation to other pages
### 4. Event Chart Component
- to display weekly agends (with weekly dates navigation)
### 5. Notification Component
- Common error handling component, when error occured while fetching data
### 6. Automated Documentation
- ng run compodoc
### 7. EN/ NL Language Support
- Users can click on EN/ NL links at the top to switch between language of the website
### 8. End-to-End Automated Testing
- Run `ng e2e` command to see the automated end to end testing