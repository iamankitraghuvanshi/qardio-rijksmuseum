import { TestBed } from '@angular/core/testing';

import { RijksmuseumDataService } from './rijksmuseum-data.service';

describe('RijksmuseumDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RijksmuseumDataService = TestBed.get(RijksmuseumDataService);
    expect(service).toBeTruthy();
  });
});
