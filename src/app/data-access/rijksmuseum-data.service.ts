import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
/** 
* @ignore
*/
@Injectable()
export class RijksmuseumDataService {
  /** 
   * @ignore
  */
  private apiKey = `yW6uq3BV`;
  /** 
   * @ignore
  */
  private culture = `en`;
  
  /**
  * The "constructor"
  *
  * @param {HttpClient} http An Http Client service
  */  
  constructor(private http: HttpClient) { 

  }

  /**
  * This method is used to get list of all the
  * Rijksmuseum painting collection
  * @example
  * getCollections()
  */
  getCollections(): Observable<any>{
    const url = `https://www.rijksmuseum.nl/api/nl/collection/?key=${this.apiKey}&format=json&imageOnly=true&q=Rembrandt&f.dating.period=17&culture=${this.culture}`;
    return this.http.get(url)
                .pipe( map(res => { return res }) );
  }

  /**
  * This method is used to get list of all the
  * Rijksmuseum agenda
  * @param {string} currentDate event date to get the list
  * Pass key for cach
  * @example
  * getAgenda('25-06-2019')
  */
  getAgenda(currentDate: String): Observable<any> {
    const url = `https://www.rijksmuseum.nl/api/nl/agenda/${currentDate}?key=${this.apiKey}&format=json&culture=${this.culture}`;
    return this.http.get(url)
                .pipe( map(res => { return res }) );
  }
}
