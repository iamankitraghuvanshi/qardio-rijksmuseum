import { Component, OnInit } from '@angular/core';
/**
 * Notification component
 */
@Component({
  selector: 'notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})

export class NotificationComponent {
   /**
  * The "constructor"
  */
  constructor() { }

}
