import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

/**
 * App component
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

/**
 * Class App Component with 
 * method switchLang()
 */
export class AppComponent {
  /**
  * The "constructor"
  *
  * @param {TranslateService} translate A Translate service
  */
  constructor(private translate: TranslateService) {
    translate.setDefaultLang('en');
  }
  
  /**
  * This method is used to switch between languages
  * @param {string} language  selected language
  * Pass language for lang switch
  * @example
  * switchLang()
  */
  switchLang(language: string) {
    this.translate.use(language);
  }
}
