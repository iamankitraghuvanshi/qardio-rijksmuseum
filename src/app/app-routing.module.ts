import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MuseumOverviewComponent } from './museum-overview/museum-overview.component';
import { MuseumEventsComponent } from './museum-events/museum-events.component';
import { MuseumHomeComponent } from './museum-home/museum-home.component';
import { EventChartComponent } from './event-chart/event-chart.component';
import { NotificationComponent } from './notification/notification.component';

/**
* Routes for the application
*/
const routes: Routes = [
  { 
    path: 'collections', 
    component: MuseumOverviewComponent 
  }, { 
    path: 'events', 
    component: MuseumEventsComponent 
  }, { 
    path: 'chart', 
    component: EventChartComponent
  }, {
    path: '', 
    component: MuseumHomeComponent
  }
];

/**
* App routing module
*/
@NgModule({
  exports: [ RouterModule ],
  imports: [
    RouterModule.forRoot(routes)
  ]
})

export class AppRoutingModule { };
/**
* Routing Components configurations
*/
export const RoutingComponents = [
  MuseumOverviewComponent, 
  MuseumHomeComponent, 
  MuseumEventsComponent,
  EventChartComponent,
  NotificationComponent
];
