import {
  TestBed,
  async,
  ComponentFixture
} from '@angular/core/testing';

import { MuseumOverviewComponent } from './museum-overview.component';
import { RijksmuseumDataService } from './../data-access/rijksmuseum-data.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'; 
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClient } from '@angular/common/http';

fdescribe('MuseumOverviewComponent', () => {
  let component: MuseumOverviewComponent;
  let rijksmuseum: RijksmuseumDataService;
  let fixture: ComponentFixture<MuseumOverviewComponent>;
  let httpMock: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ MuseumOverviewComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [RijksmuseumDataService, HttpClient]
    }).overrideComponent(MuseumOverviewComponent, {
      set: {
        providers: [RijksmuseumDataService, HttpClient]
      }
    }).compileComponents();

    rijksmuseum = TestBed.get(RijksmuseumDataService);
    httpMock = TestBed.get(HttpTestingController);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuseumOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('getCollections Method', () => {
   it('should call RijksmuseumDataService with getCollections, and return an art object', () => {
      rijksmuseum.getCollections().subscribe(data => {
        expect(data.artObjects.length).toBe(9);
      });  
    });
  });
});
