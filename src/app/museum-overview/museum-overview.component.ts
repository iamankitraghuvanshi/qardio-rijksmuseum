import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { RijksmuseumDataService } from '../data-access/rijksmuseum-data.service';
import { NotificationComponent } from '../notification/notification.component';

/**
 * Museum Overview component
 */

@Component({
  selector: 'museum-overview',
  templateUrl: './museum-overview.component.html',
  styleUrls: ['./museum-overview.component.css'],
  providers: [ RijksmuseumDataService, NotificationComponent ],
  encapsulation: ViewEncapsulation.None
})

export class MuseumOverviewComponent implements OnInit {
  /** 
   * @ignore
  */
  public artObjects: Object
  /** 
   * @ignore
  */
  public artObj = [];
  /** 
   * @ignore
  */
  errorOccured: boolean = false;
  
  /**
  * The "constructor"
  *
  * @param {RijksmuseumDataService} rijksmuseum A Rijksmuseum data service
  */
  constructor( private rijksmuseum: RijksmuseumDataService ) { 
    this.rijksmuseum = rijksmuseum;
  }
  
  /**
  * This method is used to load the method getCollections
  * for displaying all the paintings.
  * @example
  * ngOnInit()
  */
  ngOnInit() {
    this.getCollections();
  }

  /**
  * This method is used to show all the collections of paintings
  * @example
  * getCollections()
  */
  getCollections() {
    this.rijksmuseum.getCollections().subscribe(data => {
      this.artObjects = data.artObjects.map(art => {
          art.artistWithDate = art.longTitle.split('Rembrandt van Rijn,')[1];
          return art;
      });
    }, err => {
      this.errorOccured = true;
    });
  }
}
