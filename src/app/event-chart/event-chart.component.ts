import { select, scaleLinear, scaleTime, extent, axisLeft, axisBottom, line, curveCardinal } from 'd3';
import { Component, ViewEncapsulation } from "@angular/core";
import { RijksmuseumDataService } from '../data-access/rijksmuseum-data.service';
/**
 * Event Chart component
 */
@Component({
  selector: 'event-chart',
  templateUrl: './event-chart.component.html',
  styleUrls: ['./event-chart.component.css'],
  providers: [RijksmuseumDataService],
  encapsulation: ViewEncapsulation.None
})

export class EventChartComponent  {
  /** 
   * @ignore
  */
  public allEvents = [];
  /** 
   * @ignore
  */
  firstDate: Date;
  /** 
   * @ignore
  */
  lastDate: Date;

  /**
  * The "constructor"
  *
  * @param {RijksmuseumDataService} rijksmuseum A Rijksmuseum data service
  */
  constructor(private rijksmuseum: RijksmuseumDataService) {

  }
  
  /**
  * This method is used to set start date for d3 chart
  * and call the method getAgenda() with the date set previously.
  * @example
  * ngOnInit()
  */
  ngOnInit() {
    this.firstDate = new Date(2019,6,1);
    this.getAgenda(this.firstDate);
  }

  /**
  * This method is used to get event agenda
  * @param {Date} date date to be passed for API call
  * for event details
  * @example
  * getAgenda('26-06-2019')
  */
  async getAgenda(date: Date) {
    
    for(let i=1; i<=7; i++) {

      const Month = ('0' + (date.getMonth() + 1)).slice(-2);
      const Day = ('0' + date.getDate()).slice(-2);
      const dateParam = [date.getFullYear(), Month, Day].join('-');  
     
      await this.rijksmuseum.getAgenda(dateParam).subscribe((data) => {
        return this.allEvents.push({
          eventDay: i,
          eventCount: data.options.length
        });
        
      });
      
      let nextDate = new Date(date);
      nextDate.setDate(date.getDate() + 1);
      this.lastDate = date;
      date = nextDate;
    }

    setTimeout(()=> {
      this.allEvents.sort((a, b) => (a.eventDay > b.eventDay) ? 1 : -1)
      this.createChart(this.allEvents);
    }, 1000)
 }

  /**
  * This method is used to create event agenda chart with d3js
  * @param {Array} dataSet data for d3 chart
  * for event details
  * @example
  * createChart()
  */
  createChart(dataSet) {
    
    const svg = select('svg');
    svg.selectAll('*').remove();
    const width = +svg.attr('width');
    const height = +svg.attr('height');
    
    const render = data => {
      
      const title = 'Rijksmuseum Events Calendar';
      const xValue = d => d.eventDay;
      const xAxisLabel = 'Time (Day)';
      
      const yValue = d => d.eventCount;
      const circleRadius = 3;
      const yAxisLabel = 'Events Scheduled';

      const margin = { top: 60, right: 40, bottom: 88, left: 105 };
      const innerWidth = width - margin.left - margin.right;
      const innerHeight = height - margin.top - margin.bottom;
      
      const xScale = scaleTime()
        .domain([1, 7])
        .range([0, innerWidth])
        .nice();
      
      const yScale = scaleLinear()
        .domain([2, 4])
        .range([innerHeight, 0])
        .nice();
      
      const g = svg.append('g')
        .attr('transform', `translate(${margin.left},${margin.top})`);
      
      const xAxis = axisBottom(xScale)
        .tickSize(-innerHeight)
        .tickPadding(15);
      
      const yAxis = axisLeft(yScale)
        .tickSize(-innerWidth)
        .tickPadding(10);
      
      const yAxisG = g.append('g').call(yAxis);
      yAxisG.selectAll('.domain').remove();
      
      yAxisG.append('text')
          .attr('font-size', '14px')
          .attr('y', -60)
          .attr('x', -innerHeight / 2)
          .attr('fill', 'black')
          .attr('transform', `rotate(-90)`)
          .attr('text-anchor', 'middle')
          .text(yAxisLabel);
      
      const xAxisG = g.append('g').call(xAxis)
        .attr('transform', `translate(0,${innerHeight})`);
      
      xAxisG.select('.domain').remove();
      
      xAxisG.append('text')
          .attr('font-size', '14px')
          .attr('y', 80)
          .attr('x', innerWidth / 2)
          .attr('fill', 'black')
          .text(xAxisLabel);
      
      const lineGenerator = line()
        .x(d => xScale(xValue(d)))
        .y(d => yScale(yValue(d)))
        .curve(curveCardinal);
      
      g.append('path')
          .attr('fill', 'none')
          .attr('stroke', '#000')
          .attr('d', lineGenerator(data));
      
      g.append('text')
          .attr('class', 'title')
          .attr('y', -10)
          .text(title);

      g.selectAll('circle').data(data).enter().append('circle')
      .attr('cy', d => yScale(yValue(d)))
      .attr('cx', d => xScale(xValue(d)))
      .attr('r', circleRadius)
    };
    render(dataSet)
  }

  /**
  * This method is used to get the next day agenda 
  * and set the date on UI
  * @example
  * nextAgenda()
  */
  nextAgenda() {
    let nextDate = new Date(this.firstDate);
    nextDate.setDate(this.firstDate.getDate() + 7);
    this.firstDate = nextDate;
    this.getAgenda(nextDate);
  }
  
  /**
  * This method is used to get the previous dat agenda 
  * and set the date on UI
  * @example
  * previousAgenda()
  */
  previousAgenda() {
    let previousDate = new Date(this.lastDate);
    previousDate.setDate(this.lastDate.getDate() - 7)
    this.lastDate = previousDate;
    let nextDate = new Date(this.firstDate);
    nextDate.setDate(this.firstDate.getDate() - 7);
    this.firstDate = nextDate;

    this.getAgenda(this.firstDate);
  }
}