import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MuseumEventsComponent } from './museum-events.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('MuseumEventsComponent', () => {
  let component: MuseumEventsComponent;
  let fixture: ComponentFixture<MuseumEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuseumEventsComponent ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MuseumEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
