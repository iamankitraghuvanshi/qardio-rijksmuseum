import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { RijksmuseumDataService } from '../data-access/rijksmuseum-data.service';
import { NotificationComponent } from '../notification/notification.component';

/**
 * Museum Event component
 */
@Component({
  selector: 'museum-events',
  templateUrl: './museum-events.component.html',
  styleUrls: ['./museum-events.component.css'],
  providers: [ RijksmuseumDataService, NotificationComponent ],
  encapsulation: ViewEncapsulation.None
})

export class MuseumEventsComponent implements OnInit {
  /** 
   * @ignore
  */
  currentDate: Date;
  /** 
   * @ignore
  */
  disableDateBtns: boolean;
  /** 
   * @ignore
  */
  events: Object;
  /** 
   * @ignore
  */
  errorOccured: boolean = false;

  /**
  * The "constructor"
  *
  * @param {RijksmuseumDataService} rijksmuseum A Rijksmuseum data service
  */
  constructor( private rijksmuseum: RijksmuseumDataService ) { 
    this.rijksmuseum = rijksmuseum;
  }

  /**
  * This method is used to set start date for d3 chart
  * and call the method getAgenda() with the date set previously.
  * Also, disable the previous button on page load
  * @example
  * ngOnInit()
  */
  ngOnInit() {
    this.currentDate = new Date();
    this.getAgenda(this.currentDate);
    this.disableDateBtns = this.currentDate <= new Date() ? true : false;
  }
  
  /**
  * This method is used to get event agenda
  * @param {Date} date date to be passed for API call
  * for event details
  * @example
  * getAgenda('26-06-2019')
  */
  getAgenda(date: Date) {
    const Month = ('0' + (date.getMonth() + 1)).slice(-2);
    const Day = ('0' + date.getDate()).slice(-2);
    const dateParam = [date.getFullYear(), Month, Day].join('-');

    this.rijksmuseum.getAgenda(dateParam).subscribe(data => {
      if(data != '' && data != undefined && data != null) { 
        this.events = data.options;
      }
    }, err => {
      this.errorOccured = true;
    });
  }

  /**
  * This method is used to get the next day agenda 
  * and set the date on UI
  * @example
  * nextAgenda()
  */
  nextAgenda() {
    let nextDate = new Date(this.currentDate);
    nextDate.setDate(this.currentDate.getDate() + 1);
    this.currentDate = nextDate;
    this.disableDateBtns = false;

    this.getAgenda(this.currentDate);
  }

  /**
  * This method is used to get the previous dat agenda 
  * and set the date on UI
  * @example
  * previousAgenda()
  */
  previousAgenda() {
    let previousDate = new Date(this.currentDate);
    previousDate.setDate(this.currentDate.getDate() - 1);
    this.currentDate = previousDate;
    this.disableDateBtns = this.currentDate <= new Date() ? true : false;

    this.getAgenda(this.currentDate);
  }

}
