import { Component, OnInit, ViewEncapsulation } from '@angular/core';

/**
 * Museum Home component
 */
@Component({
  selector: 'museum-home',
  templateUrl: './museum-home.component.html',
  styleUrls: ['./museum-home.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class MuseumHomeComponent {
   /**
  * The "constructor"
  *
  * @param {RijksmuseumDataService} rijksmuseum A Rijksmuseum data service
  */
  constructor() { 
    
  }

}
