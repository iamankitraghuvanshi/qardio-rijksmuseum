import { AppPage } from './app.po';
import { browser, logging, by } from 'protractor';

describe('Qardio Rijksmuseum E2E automated testing', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display page heading', () => {
    page.navigateTo();
    expect(page.getTitle()).toEqual('MUSEUM');
  });

  it('should count number of possible page links on home page', () => {
    page.navigateTo();
    expect(page.countNavBarList().count()).toBe(2);
  });

  it('should check the links and open events page, come back to homepage', () => {
    page.navigateTo();
    expect(page.countNavBarList().get(0).getText()).toBe('View Events');
    expect(page.countNavBarList().get(1).getText()).toBe('Browse Collection');
    
    page.countNavBarList().get(0).click();

    expect(page.getChartViewLink().getText()).toBe("Chart View");
    expect(browser.getCurrentUrl()).toEqual("http://localhost:4200/events");

    page.getBackArrowBtn().click();
    expect(browser.getCurrentUrl()).toEqual("http://localhost:4200/");
  });

  it('should check the links and open collections page, come back to homepage', () => {
    page.navigateTo();
    expect(page.countNavBarList().get(0).getText()).toBe('View Events');
    expect(page.countNavBarList().get(1).getText()).toBe('Browse Collection');
    
    page.countNavBarList().get(1).click();
    expect(browser.getCurrentUrl()).toEqual("http://localhost:4200/collections");

    page.getBackArrowBtn().click();
    expect(browser.getCurrentUrl()).toEqual("http://localhost:4200/");
  });

  it('should open chart view page, come back to events page', () => {
    page.navigateTo();
    expect(page.countNavBarList().get(0).getText()).toBe('View Events');
  
    page.countNavBarList().get(0).click();

    expect(page.getChartViewLink().getText()).toBe("Chart View");
    expect(browser.getCurrentUrl()).toEqual("http://localhost:4200/events");

    page.getChartViewLink().click();
    expect(page.getListViewLink().getText()).toBe("List View");
    expect(browser.getCurrentUrl()).toEqual("http://localhost:4200/chart");

    page.getBackArrowBtn().click();
    expect(browser.getCurrentUrl()).toEqual("http://localhost:4200/");
  });

  it('should show individual paintings on collections page', () => {
    page.navigateTo();
    page.countNavBarList().get(1).click();
    expect(browser.getCurrentUrl()).toEqual("http://localhost:4200/collections");
    expect(page.getEachCollection().count()).toBe(10);
    page.getBackArrowBtn().click();
  });

  it('should show paintings image, with title and maker', () => {
    page.navigateTo();
    page.countNavBarList().get(1).click();
    expect(browser.getCurrentUrl()).toEqual("http://localhost:4200/collections");
    expect(page.getEachCollection().count()).toBe(10);
    expect(page.getEachCollection().get(0).all(by.css('header a img')).isDisplayed()).toBeTruthy();
    expect(page.getEachCollection().get(0).all(by.css('h3')).getText()).toEqual( [ 'Self-portrait, Rembrandt van Rijn, c. 1628' ] );
    expect(page.getEachCollection().get(0).all(by.css('p')).getText()).toEqual( [ 'Rembrandt van Rijn c. 1628' ] );
  });

  it('should show individual event listing on events page', () => {
    page.navigateTo();
    page.countNavBarList().get(0).click();
    expect(browser.getCurrentUrl()).toEqual("http://localhost:4200/events");
    expect(page.getEachEvents().count()).toBe(2);
    page.getBackArrowBtn().click();
    expect(browser.getCurrentUrl()).toEqual("http://localhost:4200/");
  });

  it('should show each day events with time, title, availibity, appropriate for', () => {
    page.navigateTo();
    page.countNavBarList().get(0).click();
    expect(browser.getCurrentUrl()).toEqual("http://localhost:4200/events");
    expect(page.getEachEvents().count()).toBe(2);
    expect(page.getEachEvents().get(0).all(by.css('.event-time h2')).getText()).toEqual( [ '11:00 - 12:00' ] );
    expect(page.getEachEvents().get(0).all(by.css('.event-details h2')).getText()).toEqual(['Guided Tour Highlights of the Rijksmuseum English']);
    expect(page.getEachEvents().get(0).all(by.css('.event-details p span')).get(0).getText()).toBe('0 of 15Sold');
    expect(page.getEachEvents().get(0).all(by.css('.event-details p span')).get(1).getText()).toBe('Individuals');
    expect(page.getEachEvents().get(0).all(by.css('.event-details p span')).get(2).getText()).toBe('English');
  });

  it('should show d3 chart for events', () => {
    page.navigateTo();
    page.countNavBarList().get(0).click();
    page.getChartViewLink().click();
    expect(browser.getCurrentUrl()).toEqual("http://localhost:4200/chart");
    expect(page.getChart().isDisplayed()).toBeTruthy();
  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
