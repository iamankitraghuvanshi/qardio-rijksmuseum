import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getTitle() {
    return element(by.css('app-root h1 a.page-title')).getText() as Promise<string>;
  }

  countNavBarList() {
    return element.all(by.css('.nav-bar-list li'));
  }

  getChartViewLink() {
    return element(by.css('.chart-view'));
  }
  
  getListViewLink() {
    return element(by.css('.list-view'));
  }

  getBackArrowBtn() {
    return element(by.css('.back-nav'));
  }

  getEachCollection() {
    return element.all(by.css('.art-object'));
  }
  
  getEachEvents() {
    return element.all(by.css('.event-individual'));
  }
  
  getChart() {
    return element(by.css('.events-chart div svg'));
  }
}
